package main

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/url"
	"path"
	"time"
)

func pageHeader(light bool, title string) []byte {
	theme := "dark"
	if light {
		theme = "light"
	}
	return []byte(`<!DOCTYPE HTML>
<html>
<head>
<title>` + title + `</title>
<link rel="stylesheet" type="text/css" href="css/medinet.css">
<link rel="stylesheet" type="text/css" href="css/holo.css">
<link rel="stylesheet" type="text/css" href="css/holo-` + theme + `.css">
</head>
<body class="holo-` + theme + `">
<div class="holo-content">
`)
}

func pageFooter() []byte {
	return []byte(`</div>
</body>
</html>`)
}

func gravatar(light bool, size int, email string) string {
	d := "https://medinet.ftp.sh/images/ic_om_sq_small_dark.png"
	if light {
		d = "https://medinet.ftp.sh/images/ic_om_sq_small_light.png"
	}
	return fmt.Sprintf("https://www.gravatar.com/avatar/%x?s=%d&d=%s", md5.Sum([]byte(email)), size, url.QueryEscape(d))
}

func formatLength(length int) string {
	d := time.Duration(time.Duration(length) * time.Second)
	return fmt.Sprintf("%d:%02d", int(d.Hours()), int(d.Minutes())%60)
}

func formatCompleted(completed int) string {
	c := time.Unix(int64(completed), 0)
	return c.Format("2006-01-02 3:04")
}

func formatRecentCompleted(completed int) string {
	c := time.Unix(int64(completed), 0)
	return c.Format("3:04 Jan 2")
}

func formatStreak(streakday int) string {
	if streakday == 0 {
		return ""
	}

	s := ""
	if streakday > 1 {
		s = "s"
	}
	return fmt.Sprintf("<p>%d day%s of meditation</p>", streakday, s)
}

func formatMessage(message string) string {
	if message == "" {
		return ""
	}
	return "<p><small>" + html.EscapeString(message) + "</small></p>"
}

func communityPage(light bool) []byte {
	var b bytes.Buffer

	b.Write(pageHeader(light, "Community"))

	recentSessions, err := db.getRecentSessions()
	if err != nil {
		log.Fatal(err)
	}

	format := `<li onclick="goToAccount('%d');">
<table border="0" cellspacing="3px" cellpadding="0px" width="100%%">
<tr>
<td width="57px" height="57px" style="margin: 0px;padding: 0px;">
<img src="%s" width="57px" height="57px" style="margin: 0px;padding: 0px;">
</td>
<td>
<div style="padding-left: 4px;"><span style="font-size: 2em;font-weight: bold;">%s</span>
%s
</div></td>
<td align="right" style="vertical-align:top;font-size: 0.85em;">
%s
</td></tr>
</table>
%s
%s
</li>`
	for _, rc := range recentSessions {
		b.WriteString(fmt.Sprintf(format, rc.AccountID, gravatar(light, 104, rc.AccountEmail), formatLength(rc.Length), html.EscapeString(rc.AccountName), formatRecentCompleted(rc.Completed), formatStreak(rc.StreakDay), formatMessage(rc.Message)))
	}

	b.Write(pageFooter())

	return b.Bytes()
}

func handleUpdateCommunity() {
	for range updateCommunity {
		ioutil.WriteFile(path.Join(config.Web, "community.html"), communityPage(false), 0655)
		ioutil.WriteFile(path.Join(config.Web, "community_light.html"), communityPage(true), 0655)
	}
}
