# MediNET
[![GoDoc](https://godoc.org/gitlab.com/tslocum/medinet?status.svg)](https://godoc.org/gitlab.com/tslocum/medinet)
[![CI status](https://gitlab.com/tslocum/medinet/badges/master/pipeline.svg)](https://gitlab.com/tslocum/medinet/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Session repository and community portal for [Meditation Assistant](https://gitlab.com/tslocum/meditationassistant)

## Configure

Example `medinet.conf`:

```yaml
timezone: "America/Los_Angeles"
om: "localhost:10800"
dbdriver: "sqlite3"
dbsource: "/home/medinet/data/medinet.db"
```

## Run

```bash
medinet -c ~/.config/medinet/medinet.conf
```

## Get Support

[Open an issue](https://gitlab.com/tslocum/meditationassistant/issues) describing your problem.

## Translate

Translation is handled [online](https://medinet.rocketnine.space/translate/).

## Contribute

 1. Read the [GitLab forking workflow guide](https://docs.gitlab.com/ee/workflow/forking_workflow.html).
 2. Fork this repository.
 3. Commit code changes to your forked repository.
 4. Submit a pull request describing your changes.

## License

**GNU GPLv3**

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
