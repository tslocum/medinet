module gitlab.com/tslocum/medinet

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/mattn/go-sqlite3 v1.14.4
	gopkg.in/yaml.v2 v2.3.0
)
